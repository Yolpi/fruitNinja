var id;
var timeLeft = 3000; 
var countdownElement = document.getElementById('countdown');

countdownElement.innerHTML = timeLeft/1000;

function updateHTML() {
    countdownElement.innerHTML = timeLeft/1000;
    if (timeLeft <= 0){
        $('#countdown').hide();                                   // cache le compte à rebours
        showObjects();                                            //les objets deviennent visibles
    };
}

function updateCountdown() {
	timeLeft = timeLeft - 1000;
  updateHTML();
}

function start() {
  id = setInterval(updateCountdown, 1000)
};